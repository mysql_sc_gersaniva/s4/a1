-- Find artists that has letter d in its name:
SELECT * FROM artists WHERE name LIKE "%d%";

-- Find songs that has a length of less than 230:
SELECT * FROM songs WHERE length < 230;


-- Join the albums and songs tables (Only show the albums that has letter a in its name)
SELECT albums.album_title, songs.song_name, songs.length FROM albums
	JOIN songs ON albums.id = songs.album_id;

-- Join artists and albums tables 
 SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id WHERE album_title LIKE "%a%";

-- Sort albums in Z-A order 
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- Join the albums and songs tables, Sort from Z-A
 SELECT * FROM albums
	JOIN songs ON albums.id = songs.album_id ORDER BY album_title DESC;
